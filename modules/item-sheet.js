/* -------------------------------------------- */

/**
 * Extend the basic ItemSheet with some very simple modifications
 */
export class MotwItemSheet extends ItemSheet {
  constructor(...args) {
    super(...args);
  }

  /**
   * Extend and override the default options used by the Simple Item Sheet
   * @returns {Object}
   */
  static get defaultOptions() {
    return mergeObject(super.defaultOptions, {
      classes: ["motw", "sheet", "item"],
      template: "systems/motw/templates/item-sheet.html",
      width: 520,
      height: 480,
    });
  }
}
